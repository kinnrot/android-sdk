package com.clustertv.ctvsdkexample;

import android.app.Application;

import com.clustertv.ctvsdk.CTVSDK;

public class ExampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // dev: "app-a65a44bf6ff7"
        // staging: "app-560011bab072"
        CTVSDK.getInstance().initialize(this, "app-a65a44bf6ff7");
    }
}
