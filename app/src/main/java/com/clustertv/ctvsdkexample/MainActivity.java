package com.clustertv.ctvsdkexample;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.clustertv.ctvsdk.core.ILoggerListener;
import com.clustertv.ctvsdk.core.Logger;
import com.inaka.galgo.Galgo;
import com.inaka.galgo.GalgoOptions;

public class MainActivity extends AppCompatActivity {
    private boolean _isDebugOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context context = this;

        getDebugButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _isDebugOn = !_isDebugOn;

                if (_isDebugOn) {
                    final GalgoOptions options = new GalgoOptions.Builder()
                            .numberOfLines(10)
                            .backgroundColor(Color.parseColor("#D9d6d6d6"))
                            .textColor(Color.BLACK)
                            .textSize(15)
                            .build();

                    Galgo.enable(context, options);

                    getDebugButton().setText(R.string.button_debug_text_hide_logs);
                }
                else {
                    Galgo.disable(context);

                    getDebugButton().setText(R.string.button_debug_text_show_logs);
                }
            }
        });

        Logger.attachListener(new ILoggerListener() {
            @Override
            public void onMessage(String message) {
                Galgo.log(message);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Galgo.disable(this);
    }

    private Button getDebugButton() {
        return (Button) findViewById(R.id.button_debug);
    }
}
