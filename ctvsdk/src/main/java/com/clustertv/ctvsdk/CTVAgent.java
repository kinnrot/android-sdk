package com.clustertv.ctvsdk;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

import com.clustertv.ctvsdk.api.ApiProxy;
import com.clustertv.ctvsdk.api.ISubscribeDeviceCallback;
import com.clustertv.ctvsdk.core.Logger;
import com.clustertv.ctvsdk.deviceinfo.DeviceInfo;
import com.clustertv.ctvsdk.deviceinfo.DeviceInfoProvider;
import com.clustertv.ctvsdk.deviceinfo.IDeviceInfoCallback;
import com.clustertv.ctvsdk.googleapi.GoogleApiProxy;
import com.clustertv.ctvsdk.localstorage.LocalStorage;
import com.clustertv.ctvsdk.models.AdBroadcast;
import com.clustertv.ctvsdk.models.FeedbackKind;
import com.clustertv.ctvsdk.serialization.ModelSerializer;
import com.clustertv.ctvsdk.shakesensor.MultiShakeSensor;
import com.clustertv.ctvsdk.time.ServerTime;
import com.google.firebase.iid.FirebaseInstanceId;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Map;

class CTVAgent implements IDeviceTokenRefreshedHandler, IMessageDataReceivedHandler, IDeviceMotionTracker {
    private final Context _context;
    private final String _appId;
    private final ApiProxy _apiProxy;
    private final Handler _uiHandler;
    private final InteractionProcessor _interactionProcessor;
    private final DeviceInfoProvider _deviceInfoProvider;
    private final ModelSerializer _modelSerializer;
    private final GoogleApiProxy _googleApiProxy;
    private final MultiShakeSensor _multiShakeSensor;
    private final LocalStorage _localStorage;

    private String _deviceId;
    private AdBroadcast _currentAdBroadcast;
    private final Runnable _endHandlingShakesTask = new Runnable() {
        @Override
        public void run() {
            endHandlingShakes();
        }
    };
    private boolean _isDeviceInMotion;
    private final Runnable _beginHandlingShakesTask = new Runnable() {
        @Override
        public void run() {
            beginHandlingShakes();
        }
    };

    private BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (CTVIntent.CTV_NOTIFICATION_TAPPED.equals(intent.getAction())) {
                String adBroadcastJson = intent.getStringExtra("adBroadcastJson");
                AdBroadcast adBroadcast = _modelSerializer.deserializeAdBroadcast(adBroadcastJson);

                if (adBroadcast != null) {
                    _interactionProcessor.triggerAdAction(adBroadcast);
                    sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.ACTION_TRIGGERED);
                }
            }
        }
    };

    CTVAgent(Context context, String appId) {
        _context = context;
        _appId = appId;

        _apiProxy = new ApiProxy(context);
        _uiHandler = new Handler();
        _interactionProcessor = new InteractionProcessor(context);
        _deviceInfoProvider = new DeviceInfoProvider(context);
        _modelSerializer = new ModelSerializer();
        _multiShakeSensor = new MultiShakeSensor(context);
        _localStorage = new LocalStorage(context);

        _deviceId = _localStorage.loadDeviceId();

        registerBroadcastReceiver();

        _googleApiProxy = new GoogleApiProxy(context);
        _googleApiProxy.connect();

        informDeviceTokenRefreshed(FirebaseInstanceId.getInstance().getToken());
    }

    void shutdown() {
        _googleApiProxy.disconnect();
        _context.unregisterReceiver(_broadcastReceiver);
    }

    @Override
    public void onMessageDataReceived(final Map<String, String> messageData) {
        _uiHandler.post(new Runnable() {
            @Override
            public void run() {
                informMessageDataReceived(messageData);
            }
        });
    }

    @Override
    public void onDeviceTokenRefreshed(final String deviceToken) {
        _uiHandler.post(new Runnable() {
            @Override
            public void run() {
                informDeviceTokenRefreshed(deviceToken);
            }
        });
    }

    @Override
    public void onDeviceInMotionUpdated(final boolean isInMotion) {
        _uiHandler.post(new Runnable() {
            @Override
            public void run() {
                informDeviceInMotionUpdated(isInMotion);
            }
        });
    }

    private void informDeviceInMotionUpdated(boolean isInMotion) {
        Logger.info(String.format("Device motion updated. is in motion: %s", isInMotion));

        _isDeviceInMotion = isInMotion;
    }

    private void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CTVIntent.CTV_NOTIFICATION_TAPPED);

        _context.registerReceiver(_broadcastReceiver, intentFilter);
    }

    private void informMessageDataReceived(Map<String, String> messageData) {
        if (!messageData.containsKey("isCTV")) {
            return;
        }

        Logger.info(String.format("Message data received: %s", messageData));

        String notificationType = messageData.get("type");
        if (notificationType != null) {
            if (notificationType.equals("HEARTBEAT")) {
                Logger.info("Heartbeat received");
                return;
            }
        }


        if (_currentAdBroadcast != null) {
            _uiHandler.removeCallbacks(_beginHandlingShakesTask);
            _uiHandler.removeCallbacks(_endHandlingShakesTask);
            endHandlingShakes();
        }

        String adBroadcastJson = messageData.get("adBroadcast");
        AdBroadcast adBroadcast = _modelSerializer.deserializeAdBroadcast(adBroadcastJson);
        if (adBroadcast == null) {
            return;
        }

        sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.NOTIFICATION_RECEIVED);

        DateTime now = ServerTime.now();

        if (now.isAfter(adBroadcast.getAdEndTime())) {
            Logger.warn(String.format("Received an expired ad broadcast. ad broadcast id: %s", adBroadcast.getAdBroadcastId()));
            sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.AD_BROADCAST_EXPIRED);
            return;
        }

        _currentAdBroadcast = adBroadcast;

        if (now.isBefore(adBroadcast.getAdStartTime())) {
            long startsInMs = new Duration(now, adBroadcast.getAdStartTime()).getMillis();
            Logger.info(String.format("Received an ad broadcast ahead of time, scheduling in %s ms. ad broadcast id: %s", startsInMs, adBroadcast.getAdBroadcastId()));
            _uiHandler.postDelayed(_beginHandlingShakesTask, startsInMs);
        }
        else {
            beginHandlingShakes();
        }
    }

    private void informDeviceTokenRefreshed(final String deviceToken) {
        if (deviceToken == null) {
            return;
        }

        _deviceInfoProvider.getDeviceInfoAsync(new IDeviceInfoCallback() {
            @Override
            public void onDeviceInfo(final DeviceInfo deviceInfo) {
                _uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        subscribeDevice(deviceToken, deviceInfo);
                    }
                });
            }
        });
    }

    private void subscribeDevice(String deviceToken, DeviceInfo deviceInfo) {
        _apiProxy.subscribeDevice(_appId, _deviceId, deviceToken, deviceInfo, new ISubscribeDeviceCallback() {
            @Override
            public void onCallback(final String deviceId) {
                _uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateDeviceId(deviceId);
                    }
                });
            }
        });
    }

    private void updateDeviceId(String deviceId) {
        _deviceId = deviceId;
        _localStorage.storeDeviceId(_deviceId);
    }

    private void onDeviceShaken() {
        AdBroadcast adBroadcast = _currentAdBroadcast;

        if (_isDeviceInMotion) {
            Logger.info(String.format("Device is shaken but it is in motion, ignoring it. ad broadcast id: %s", adBroadcast.getAdBroadcastId()));
            sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.MOVING_DEVICE_SHAKEN);
            return;
        }

        sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.DEVICE_SHAKEN);

        _uiHandler.removeCallbacks(_endHandlingShakesTask);
        endHandlingShakes();

        if (isDeviceLocked()) {
            sendNotification(adBroadcast);
        }
        else {
            playShakeSound();
            _interactionProcessor.triggerAdAction(adBroadcast);
            sendFeedback(adBroadcast.getAdBroadcastId(), FeedbackKind.ACTION_TRIGGERED);
        }
    }

    private void playShakeSound() {
        MediaPlayer player = MediaPlayer.create(_context, R.raw.ctv_shake_sound);
        player.setLooping(false);
        player.start();
    }

    private synchronized void beginHandlingShakes() {
        Logger.info("Begin handling shakes");

        _multiShakeSensor.start(new MultiShakeSensor.IMultiShakeHandler() {
            @Override
            public void onMultiShake() {
                onDeviceShaken();
            }
        });

        long expiresInMs = new Duration(ServerTime.now(), _currentAdBroadcast.getAdEndTime()).getMillis();
        _uiHandler.postDelayed(_endHandlingShakesTask, expiresInMs);
    }

    private synchronized void endHandlingShakes() {
        Logger.info("End handling shakes");

        _multiShakeSensor.stop();

        _currentAdBroadcast = null;
    }

    private boolean isDeviceLocked() {
        KeyguardManager keyguardManager = (KeyguardManager) _context.getSystemService(Context.KEYGUARD_SERVICE);
        return keyguardManager.inKeyguardRestrictedInputMode();
    }

    private void sendNotification(AdBroadcast adBroadcast) {
        Intent intent = new Intent(CTVIntent.CTV_NOTIFICATION_TAPPED);
        intent.putExtra("adBroadcastJson", _modelSerializer.serializeAdBroadcast(adBroadcast));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(_context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Notification notification = new NotificationCompat.Builder(_context)
                .setSmallIcon(R.drawable.ic_stat_ctv_logo_small)
                .setContentTitle(adBroadcast.getNotificationTitle())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(adBroadcast.getNotificationTitle()))
                .setContentText(adBroadcast.getNotificationBody())
                .setTicker(adBroadcast.getNotificationTitle())
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(_context.getResources(), R.drawable.ic_stat_ctv_logo_small))
                .setSound(Uri.parse("android.resource://" + _context.getPackageName() + "/" + R.raw.ctv_shake_sound))
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    private void sendFeedback(String adBroadcastId, FeedbackKind feedbackKind) {
        if (_deviceId != null) {
            Logger.info(String.format("Sending feedback. ad broadcast id: %s, feedback kind: %s", adBroadcastId, feedbackKind));
            _apiProxy.sendFeedback(_appId, _deviceId, adBroadcastId, feedbackKind);
        }
        else {
            Logger.warn(String.format("Cannot send feedback, no device id yet. ad broadcast id: %s, feedback kind: %s", adBroadcastId, feedbackKind));
        }
    }
}
