package com.clustertv.ctvsdk;

import android.content.Context;

public class CTVSDK {
    private static CTVSDK _instance = new CTVSDK();

    private CTVAgent _agent;

    private CTVSDK() {
    }

    public static CTVSDK getInstance() {
        return _instance;
    }

    public void initialize(Context context, String appId) {
        _agent = new CTVAgent(context.getApplicationContext(), appId);
    }

    public void shutdown() {
        if (_agent != null) {
            _agent.shutdown();
            _agent = null;
        }
    }

    public IMessageDataReceivedHandler getMessageDataReceivedHandler() {
        return _agent;
    }

    public IDeviceTokenRefreshedHandler getDeviceTokenRefreshedHandler() {
        return _agent;
    }

    public IDeviceMotionTracker getDeviceMotionTracker() {
        return _agent;
    }
}