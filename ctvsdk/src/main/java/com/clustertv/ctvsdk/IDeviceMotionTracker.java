package com.clustertv.ctvsdk;

public interface IDeviceMotionTracker {
    void onDeviceInMotionUpdated(boolean isInMotion);
}
