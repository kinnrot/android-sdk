package com.clustertv.ctvsdk;

public interface IDeviceTokenRefreshedHandler {
    void onDeviceTokenRefreshed(String deviceToken);
}
