package com.clustertv.ctvsdk;

import java.util.Map;

public interface IMessageDataReceivedHandler {
    void onMessageDataReceived(Map<String, String> messageData);
}
