package com.clustertv.ctvsdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.clustertv.ctvsdk.core.Logger;
import com.clustertv.ctvsdk.models.AdBroadcast;

import java.util.List;

class InteractionProcessor {
    private Context _context;

    public InteractionProcessor(Context context) {
        _context = context;
    }

    public void triggerAdAction(AdBroadcast adBroadcast) {
        String actionType = adBroadcast.getActionType();

        switch (actionType) {
            case "app_page":
                openInPlayStore(adBroadcast.getPlayStoreAppId());
                break;
            case "web_page":
                openInBrowser(adBroadcast.getWebPageUrl());
                break;
            default:
                Logger.info(String.format("Unknown action type: %s", actionType));
                break;
        }
    }

    public void openInPlayStore(String playStoreAppId) {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", playStoreAppId)));
        boolean isMarketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = _context.getPackageManager().queryIntentActivities(rateIntent, 0);

        for (ResolveInfo otherApp : otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {
                ActivityInfo otherAppActivity = otherApp.activityInfo;

                ComponentName componentName = new ComponentName(otherAppActivity.applicationInfo.packageName, otherAppActivity.name);
                rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                rateIntent.setComponent(componentName);

                Logger.info(String.format("Opening play store app: %s", playStoreAppId));

                _context.startActivity(rateIntent);
                isMarketFound = true;
                break;
            }
        }

        // if GP not present on device, open web browser
        if (!isMarketFound) {
            String url = String.format("https://play.google.com/store/apps/details?id=%s", playStoreAppId);
            openInBrowser(url);
        }
    }

    public void openInBrowser(String url) {
        Logger.info(String.format("Opening browser url: %s", url));
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(browserIntent);
    }
}
