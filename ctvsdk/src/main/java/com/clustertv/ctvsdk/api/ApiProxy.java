package com.clustertv.ctvsdk.api;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.clustertv.ctvsdk.BuildConfig;
import com.clustertv.ctvsdk.core.DateTimes;
import com.clustertv.ctvsdk.core.Logger;
import com.clustertv.ctvsdk.deviceinfo.DeviceInfo;
import com.clustertv.ctvsdk.models.FeedbackKind;
import com.clustertv.ctvsdk.time.ServerTime;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiProxy {
    private static String TAG = "ApiProxy";
    private static String DEFAULT_API_BASE_URL = "https://ctvmsdks.herokuapp.com/api/";

    private final RequestQueue _requestQueue;

    private Uri _apiBaseUrl = null;

    public ApiProxy(Context context) {
        _requestQueue = Volley.newRequestQueue(context);

        assignApiBaseUrl(context);
    }

    public void subscribeDevice(String appId, String existingDeviceId, final String deviceToken, final DeviceInfo deviceInfo, final ISubscribeDeviceCallback callback) {
        Uri url = Uri.withAppendedPath(_apiBaseUrl, "devices");

        JSONObject payload = new JSONObject();

        try {
            payload.put("sdkVersion", BuildConfig.VERSION_NAME);
            payload.put("appId", appId);
            payload.put("deviceId", existingDeviceId);
            payload.put("platform", "ANDROID");
            payload.put("token", deviceToken);
            payload.put("adId", deviceInfo.getAdId());
            payload.put("deviceModel", deviceInfo.getModelName());
            payload.put("deviceOs", deviceInfo.getOsVersion());
            payload.put("countryCode", deviceInfo.getCountryCode());
        }
        catch (JSONException e) {
            Logger.error("Failed to subscribe device due to serialization error.", e);
            return;
        }

        JsonObjectRequest request = new TimeSyncingRequest(Request.Method.POST, url.toString(), payload, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Logger.info("Device subscribed successfully.");

                try {
                    String deviceId = response.getString("deviceId");
                    callback.onCallback(deviceId);
                }
                catch (JSONException e) {
                    Logger.error("Failed to deserialize subscribe device response.", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.error("Failed to subscribe device.", error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        _requestQueue.add(request);
    }

    public void sendFeedback(String appId, String deviceId, String adBroadcastId, FeedbackKind kind) {
        Uri url = Uri.withAppendedPath(_apiBaseUrl, "feedbacks");

        JSONObject payload = new JSONObject();

        try {
            payload.put("deviceId", deviceId);
            payload.put("appId", appId);
            payload.put("adBroadcastId", adBroadcastId);
            payload.put("feedbackKind", kind.getValue());
            payload.put("occurredAt", ServerTime.now().getMillis() / 1000);
        }
        catch (JSONException e) {
            Logger.error("Failed to build json payload", e);
            return;
        }

        JsonObjectRequest request = new TimeSyncingRequest(Request.Method.POST, url.toString(), payload, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Logger.info("Feedback sent successfully.");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.error("Failed to send feedback.", error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        _requestQueue.add(request);
    }

    private void assignApiBaseUrl(Context context) {
        String apiBaseUrl = DEFAULT_API_BASE_URL;

        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle metaData = applicationInfo.metaData;
            String anApiBaseUrl = metaData.getString("ctv_api_base_url");

            if (anApiBaseUrl != null) {
                apiBaseUrl = anApiBaseUrl;
            }
        }
        catch (PackageManager.NameNotFoundException ignored) {
        }

        _apiBaseUrl = Uri.parse(apiBaseUrl);
    }
}
