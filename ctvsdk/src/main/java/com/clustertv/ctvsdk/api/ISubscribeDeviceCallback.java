package com.clustertv.ctvsdk.api;

public interface ISubscribeDeviceCallback {
    void onCallback(String deviceId);
}
