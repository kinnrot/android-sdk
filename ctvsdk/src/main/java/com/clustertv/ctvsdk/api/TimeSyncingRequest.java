package com.clustertv.ctvsdk.api;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.clustertv.ctvsdk.core.DateTimes;
import com.clustertv.ctvsdk.core.Logger;
import com.clustertv.ctvsdk.time.ServerTime;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class TimeSyncingRequest extends JsonObjectRequest {
    public TimeSyncingRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public TimeSyncingRequest(String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
    }


    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        trySyncServerTime(response);
        return super.parseNetworkResponse(response);
    }

    private void trySyncServerTime(NetworkResponse response) {
        String serverTimeHeader = response.headers.get("X-Server-Time");

        if (serverTimeHeader != null) {
            DateTime serverTime = DateTimes.fromTimestamp(serverTimeHeader);

            DateTime networkAwareServerTime = serverTime.minus(response.networkTimeMs / 2);
            ServerTime.setTime(networkAwareServerTime);

            Logger.info(String.format("Server time synced. time: %s", ServerTime.now().toString()));
        }
    }
}
