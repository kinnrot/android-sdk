package com.clustertv.ctvsdk.core;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

public class DateTimes {
    public static long absDeltaInMs(DateTime x, DateTime y) {
        return Math.abs(deltaInMs(x, y));
    }

    public static long deltaInMs(DateTime x, DateTime y) {
        if (x.isAfter(y)) {
            return interval(x, y);
        }

        return -interval(y, x);
    }

    public static long interval(DateTime later, DateTime before) {
        return new Interval(before, later).toDurationMillis();
    }

    public static boolean isBeforeInclusive(DateTime time, DateTime endTime) {
        return !time.isAfter(endTime);
    }

    public static DateTime toUTC(DateTime x) {
        return x.withZone(DateTimeZone.UTC);
    }

    public static DateTime markAsUTC(DateTime x) {
        return x.withZoneRetainFields(DateTimeZone.UTC);
    }

    public static DateTime minValue() {
        return new DateTime(0);
    }

    public static DateTime fromTimestamp(String timestamp) {
        return new DateTime(Long.valueOf(String.format("%s000", timestamp)));
    }

    public static DateTime fromTimestamp(long serverTime) {
        return fromTimestamp(Long.toString(serverTime));
    }
}
