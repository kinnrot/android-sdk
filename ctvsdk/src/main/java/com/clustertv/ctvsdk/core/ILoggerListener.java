package com.clustertv.ctvsdk.core;

public interface ILoggerListener {
    void onMessage(String message);
}
