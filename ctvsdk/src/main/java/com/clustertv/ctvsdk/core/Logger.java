package com.clustertv.ctvsdk.core;

import android.util.Log;

public class Logger {
    private static String TAG = "CTVSDK";

    private static volatile ILoggerListener _loggerListener;

    public static synchronized void attachListener(ILoggerListener loggerListener) {
        _loggerListener = loggerListener;
    }

    public static void debug(String message) {
        Log.d(TAG, message);
        notifyListener(String.format("[DEBUG] %s", message));
    }

    public static void info(String message) {
        Log.i(TAG, message);
        notifyListener(String.format("[INFO] %s", message));
    }

    public static void warn(String message) {
        warn(message, null);
    }

    public static void warn(String message, Throwable error) {
        Log.w(TAG, message, error);
        notifyListener(String.format("[WARN] %s", message));
    }

    public static void error(String message) {
        error(message, null);
    }

    public static void error(String message, Throwable error) {
        Log.e(TAG, message, error);
        notifyListener(String.format("[ERROR] %s", message));
    }

    private static synchronized void notifyListener(String message) {
        if (_loggerListener != null) {
            try{
                _loggerListener.onMessage(message);
            }
            catch (Throwable error){
                Log.e(TAG, "Failed to notify logger listener.", error);
            }
        }
    }
}
