package com.clustertv.ctvsdk.deviceinfo;

public class DeviceInfo {
    private final String _adId;
    private final String _modelName;
    private final String _osVersion;
    private final String _countryCode;

    DeviceInfo(String adId, String modelName, String osVersion, String countryCode) {
        _adId = adId;
        _modelName = modelName;
        _osVersion = osVersion;
        _countryCode = countryCode;
    }

    public String getAdId() {
        return _adId;
    }

    public String getModelName() {
        return _modelName;
    }

    public String getOsVersion() {
        return _osVersion;
    }

    public String getCountryCode() {
        return _countryCode;
    }
}
