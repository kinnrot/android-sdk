package com.clustertv.ctvsdk.deviceinfo;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.clustertv.ctvsdk.core.Logger;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class DeviceInfoProvider {
    private final Context _context;
    private final RequestQueue _requestQueue;

    public DeviceInfoProvider(Context context) {
        _context = context;
        _requestQueue = Volley.newRequestQueue(context);
    }

    public void getDeviceInfoAsync(final IDeviceInfoCallback callback) {
        AsyncTask.SERIAL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                String modelName = Build.MODEL;
                String osVersion = Build.VERSION.RELEASE;
                String adId = getAdId();
                String countryCode = getCountryCode();

                callback.onDeviceInfo(new DeviceInfo(adId, modelName, osVersion, countryCode));
            }
        });
    }

    private String getCountryCode() {
        TelephonyManager telephonyManager = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = telephonyManager.getNetworkCountryIso();

        if (countryCode == null || countryCode.isEmpty()) {
            countryCode = resolveCountryCodeByIp();
        }

        return countryCode != null ? countryCode.toUpperCase() : null;
    }

    private String resolveCountryCodeByIp() {
        RequestFuture<JSONObject> respFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://ip-api.com/json", null, respFuture, respFuture);
        request.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        _requestQueue.add(request);

        try {
            JSONObject resp = respFuture.get();
            return resp.getString("countryCode");
        }
        catch (Exception e) {
            return null;
        }
    }

    private String getAdId() {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(_context).getId();
        }
        catch (Throwable t) {
            Logger.warn("Failed to get ad id", t);
        }

        return null;
    }
}
