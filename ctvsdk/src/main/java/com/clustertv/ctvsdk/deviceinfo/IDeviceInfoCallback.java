package com.clustertv.ctvsdk.deviceinfo;

public interface IDeviceInfoCallback {
    void onDeviceInfo(DeviceInfo deviceInfo);
}
