package com.clustertv.ctvsdk.googleapi;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.clustertv.ctvsdk.services.ActivityRecognitionService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;

public class GoogleApiProxy implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final GoogleApiClient _apiClient;
    private final PendingIntent _activityUpdatesIntent;

    public GoogleApiProxy(Context context) {
        _apiClient = new GoogleApiClient.Builder(context)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        _activityUpdatesIntent = newActivityUpdatesIntent(context);
    }

    public void connect() {
        _apiClient.connect();
    }

    public void disconnect() {
        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(_apiClient, _activityUpdatesIntent);
        _apiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(_apiClient, 5000, _activityUpdatesIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private PendingIntent newActivityUpdatesIntent(Context context) {
        return PendingIntent.getService(context, 0, new Intent(context, ActivityRecognitionService.class), PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
