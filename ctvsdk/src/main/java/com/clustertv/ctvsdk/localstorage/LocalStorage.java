package com.clustertv.ctvsdk.localstorage;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorage {
    private static final String SHARED_PREFERENCES_NAME = "ctvsdk_preferences";
    private static final String DEVICE_ID_KEY = "device_id";

    private final SharedPreferences _sharedPreferences;

    public LocalStorage(Context context) {
        _sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public synchronized void storeDeviceId(String deviceId) {
        SharedPreferences.Editor editor = _sharedPreferences.edit();
        editor.putString(DEVICE_ID_KEY, deviceId);
        editor.commit();
    }

    public synchronized String loadDeviceId() {
        return _sharedPreferences.getString(DEVICE_ID_KEY, null);
    }
}
