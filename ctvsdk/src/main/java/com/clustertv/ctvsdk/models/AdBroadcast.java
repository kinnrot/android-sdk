package com.clustertv.ctvsdk.models;

import com.clustertv.ctvsdk.serialization.UnixTimestampDeserializer;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.joda.time.DateTime;

import java.io.Serializable;

public class AdBroadcast implements Serializable {
    private String _adBroadcastId;
    private String _adId;
    private DateTime _adStartTime;
    private Integer _adDurationSeconds;
    private String _campaignId;
    private String _actionType;
    private String _webPageUrl;
    private String _playStoreAppId;
    private String _notificationTitle;
    private String _notificationBody;

    @JsonCreator
    public AdBroadcast(@JsonProperty("adBroadcastId") String adBroadcastId,
                       @JsonProperty("adId") String adId,
                       @JsonProperty("adStartTime") DateTime adStartTime,
                       @JsonProperty("adDurationSeconds") Integer adDurationSeconds,
                       @JsonProperty("campaignId") String campaignId,
                       @JsonProperty("actionType") String actionType,
                       @JsonProperty("webPageUrl") String webPageUrl,
                       @JsonProperty("playStoreAppId") String playStoreAppId,
                       @JsonProperty("notificationTitle") String notificationTitle,
                       @JsonProperty("notificationBody") String notificationBody) {
        _adBroadcastId = adBroadcastId;
        _adId = adId;
        _adStartTime = adStartTime;
        _adDurationSeconds = adDurationSeconds;
        _campaignId = campaignId;
        _actionType = actionType;
        _webPageUrl = webPageUrl;
        _playStoreAppId = playStoreAppId;
        _notificationTitle = notificationTitle;
        _notificationBody = notificationBody;
    }

    public String getAdId() {
        return _adId;
    }

    @JsonDeserialize(using = UnixTimestampDeserializer.class)
    public DateTime getAdStartTime() {
        return _adStartTime;
    }

    public Integer getAdDurationSeconds() {
        return _adDurationSeconds;
    }

    public DateTime getAdEndTime() {
        return getAdStartTime().plusSeconds(getAdDurationSeconds());
    }

    public String getCampaignId() {
        return _campaignId;
    }

    public String getActionType() {
        return _actionType;
    }

    public String getWebPageUrl() {
        return _webPageUrl;
    }

    public String getPlayStoreAppId() {
        return _playStoreAppId;
    }

    public String getNotificationTitle() {
        return _notificationTitle;
    }

    public String getNotificationBody() {
        return _notificationBody;
    }

    public String getAdBroadcastId() {
        return _adBroadcastId;
    }
}
