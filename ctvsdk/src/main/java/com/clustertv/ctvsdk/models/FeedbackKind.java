package com.clustertv.ctvsdk.models;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum FeedbackKind implements Serializable {
    NOTIFICATION_RECEIVED("NOTIFICATION_RECEIVED"),
    DEVICE_SHAKEN("DEVICE_SHAKEN"),
    ACTION_TRIGGERED("ACTION_TRIGGERED"),
    AD_BROADCAST_EXPIRED("AD_BROADCAST_EXPIRED"),
    MOVING_DEVICE_SHAKEN("MOVING_DEVICE_SHAKEN");

    private final String _value;

    FeedbackKind(String value) {
        _value = value;
    }

    @JsonValue
    public String getValue() {
        return _value;
    }
}
