package com.clustertv.ctvsdk.serialization;

import com.clustertv.ctvsdk.core.Logger;
import com.clustertv.ctvsdk.models.AdBroadcast;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import java.io.IOException;

public class ModelSerializer {
    private final ObjectMapper _objectMapper;

    public ModelSerializer() {
        _objectMapper = new ObjectMapper()
                .registerModule(new JodaModule())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public String serializeAdBroadcast(AdBroadcast adBroadcast) {
        try {
            return _objectMapper.writeValueAsString(adBroadcast);
        }
        catch (IOException e) {
            Logger.error("Failed to serialize ad broadcast", e);
            return null;
        }
    }

    public AdBroadcast deserializeAdBroadcast(String adBroadcastJson) {
        try {
            return _objectMapper.readValue(adBroadcastJson, AdBroadcast.class);
        }
        catch (IOException e) {
            Logger.error("Failed to deserialize ad broadcast", e);
            return null;
        }
    }
}
