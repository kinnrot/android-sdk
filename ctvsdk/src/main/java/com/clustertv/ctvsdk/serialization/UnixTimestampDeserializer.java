package com.clustertv.ctvsdk.serialization;

import com.clustertv.ctvsdk.core.DateTimes;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.joda.time.DateTime;

import java.io.IOException;

public class UnixTimestampDeserializer extends JsonDeserializer<DateTime> {
    @Override
    public DateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String timestamp = jp.getText().trim();
        try {
            return DateTimes.fromTimestamp(timestamp);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }
}