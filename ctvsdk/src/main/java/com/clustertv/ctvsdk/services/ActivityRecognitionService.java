package com.clustertv.ctvsdk.services;

import android.app.IntentService;
import android.content.Intent;

import com.clustertv.ctvsdk.CTVSDK;
import com.clustertv.ctvsdk.IDeviceMotionTracker;
import com.clustertv.ctvsdk.core.Logger;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ActivityRecognitionService extends IntentService {
    private final Set<Integer> _motionActivityTypes = new HashSet<>(Arrays.asList(
            DetectedActivity.IN_VEHICLE,
            DetectedActivity.ON_BICYCLE,
            DetectedActivity.ON_FOOT,
            DetectedActivity.RUNNING,
            DetectedActivity.WALKING));

    public ActivityRecognitionService() {
        super("ActivityRecognitionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            onProbableActivitiesDetected(result.getProbableActivities());
        }
    }

    private void onProbableActivitiesDetected(List<DetectedActivity> activities) {
        if (activities.isEmpty()) {
            return;
        }

        boolean isInMotion = checkIfDeviceInMotion(activities);

        IDeviceMotionTracker tracker = CTVSDK.getInstance().getDeviceMotionTracker();

        if (tracker != null) {
            Logger.info("Probable activities detected.");
            tracker.onDeviceInMotionUpdated(isInMotion);
        }
    }

    private boolean checkIfDeviceInMotion(List<DetectedActivity> activities) {
        for (DetectedActivity activity : activities) {
            if (_motionActivityTypes.contains(activity.getType()) && activity.getConfidence() > 75) {
                return true;
            }
        }

        return false;
    }
}
