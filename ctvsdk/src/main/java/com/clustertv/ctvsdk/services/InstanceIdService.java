package com.clustertv.ctvsdk.services;

import com.clustertv.ctvsdk.CTVSDK;
import com.clustertv.ctvsdk.IDeviceTokenRefreshedHandler;
import com.clustertv.ctvsdk.core.Logger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class InstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        IDeviceTokenRefreshedHandler handler = CTVSDK.getInstance().getDeviceTokenRefreshedHandler();

        if (handler != null) {
            Logger.info("Device token refreshed.");
            handler.onDeviceTokenRefreshed(FirebaseInstanceId.getInstance().getToken());
        }
    }
}
