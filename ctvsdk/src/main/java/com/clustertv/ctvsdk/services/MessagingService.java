package com.clustertv.ctvsdk.services;

import com.clustertv.ctvsdk.CTVSDK;
import com.clustertv.ctvsdk.IMessageDataReceivedHandler;
import com.clustertv.ctvsdk.core.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> messageData = remoteMessage.getData();

        IMessageDataReceivedHandler handler = CTVSDK.getInstance().getMessageDataReceivedHandler();

        if (handler != null) {
            Logger.info("Remote message received.");
            handler.onMessageDataReceived(messageData);
        }
    }
}
