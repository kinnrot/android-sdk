package com.clustertv.ctvsdk.shakesensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;

import com.clustertv.ctvsdk.core.Logger;

public class MultiShakeSensor {
    private final SensorManager _sensorManager;
    private final ShakeSensorEventListener _sensorListener;

    private int _shakeCount = 0;
    private long _firstShakeTime = 0;
    private long _lastShakeTime = 0;

    private IMultiShakeHandler _multiShakeHandler;

    public MultiShakeSensor(Context context) {
        _sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        _sensorListener = newShakeSensorEventListener();
    }

    public void start(IMultiShakeHandler handler) {
        _multiShakeHandler = handler;
        _sensorManager.registerListener(_sensorListener, _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
    }

    public void stop() {
        _sensorManager.unregisterListener(_sensorListener);
    }

    @NonNull
    private ShakeSensorEventListener newShakeSensorEventListener() {
        ShakeSensorEventListener shakeSensorEventListener = new ShakeSensorEventListener();
        shakeSensorEventListener.setShakeHandler(new ShakeSensorEventListener.IShakeHandler() {
            @Override
            public void onShake() {
                onDeviceShaken();
            }
        });
        return shakeSensorEventListener;
    }

    private void onDeviceShaken() {
        long now = System.currentTimeMillis();

        if (_firstShakeTime == 0) {
            _firstShakeTime = now;
            _lastShakeTime = now;
        }

        long fromLastShakeMs = now - _lastShakeTime;

        if (fromLastShakeMs < 1500) {
            _lastShakeTime = now;
            _shakeCount++;

            if (_shakeCount >= 2) {
                long totalDuration = now - _firstShakeTime;
                if (totalDuration < 3000) {
                    Logger.info("A MULTI SHAKE!");
                    _multiShakeHandler.onMultiShake();
                    resetSensorState();
                }
            }
        }
        else {
            resetSensorState();
        }
    }

    private void resetSensorState() {
        _shakeCount = 0;
        _firstShakeTime = 0;
        _lastShakeTime = 0;
    }

    public interface IMultiShakeHandler {
        void onMultiShake();
    }
}
