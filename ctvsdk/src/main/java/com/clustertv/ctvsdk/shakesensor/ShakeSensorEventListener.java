package com.clustertv.ctvsdk.shakesensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.clustertv.ctvsdk.core.Logger;


public class ShakeSensorEventListener implements SensorEventListener {
    private static final double MIN_FORCE = 40;
    private static final int MIN_DIRECTION_CHANGE = 3;

    private static final int MAX_PAUSE_BETWEEN_DIRECTION_CHANGE_MS = 300;
    private static final int MAX_TOTAL_DURATION_OF_SHAKE_MS = 1000;

    private long _firstDirectionChangeTime = 0;
    private long _lastDirectionChangeTime;
    private int _directionChangeCount = 0;

    private float _lastX = 0;
    private float _lastY = 0;
    private float _lastZ = 0;

    private IShakeHandler _shakeHandler;

    public void setShakeHandler(IShakeHandler hander) {
        _shakeHandler = hander;
    }

    @Override
    public void onSensorChanged(SensorEvent se) {
        // get sensor data
        float x = se.values[SensorManager.DATA_X];
        float y = se.values[SensorManager.DATA_Y];
        float z = se.values[SensorManager.DATA_Z];

        float totalMovement = Math.abs(x + y + z - _lastX - _lastY - _lastZ);

//        Logger.info(String.format("shake sensor event. total movement: %s", totalMovement));

        if (totalMovement > MIN_FORCE) {
            long now = System.currentTimeMillis();

            if (_firstDirectionChangeTime == 0) {
                _firstDirectionChangeTime = now;
                _lastDirectionChangeTime = now;
            }

            long fromLastChangeMs = now - _lastDirectionChangeTime;

            if (fromLastChangeMs < MAX_PAUSE_BETWEEN_DIRECTION_CHANGE_MS) {
                _lastDirectionChangeTime = now;
                _directionChangeCount++;

                // store last sensor data
                _lastX = x;
                _lastY = y;
                _lastZ = z;

                // check how many movements are so far
                if (_directionChangeCount >= MIN_DIRECTION_CHANGE) {
                    // check total duration
                    long totalDuration = now - _firstDirectionChangeTime;

                    if (totalDuration < MAX_TOTAL_DURATION_OF_SHAKE_MS) {
                        Logger.info("A SHAKE!");
                        _shakeHandler.onShake();
                        resetShakeListenerState();
                    }
                }
            }
            else {
                resetShakeListenerState();
            }
        }
    }

    /**
     * Resets the shake parameters to their default values.
     */
    private void resetShakeListenerState() {
        _firstDirectionChangeTime = 0;
        _directionChangeCount = 0;
        _lastDirectionChangeTime = 0;
        _lastX = 0;
        _lastY = 0;
        _lastZ = 0;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public interface IShakeHandler {
        void onShake();
    }
}
