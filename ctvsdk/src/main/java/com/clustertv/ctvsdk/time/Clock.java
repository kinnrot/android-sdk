package com.clustertv.ctvsdk.time;

import com.clustertv.ctvsdk.core.DateTimes;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class Clock {
    private DateTime _time;
    private long _delta;
    private boolean _isFrozen;

    public Clock() {
        _time = getRealNowInUtc();
        _delta = 0;
        _isFrozen = false;
    }

    public void freeze() {
        _isFrozen = true;
    }

    public void unfreeze() {
        _isFrozen = false;
    }

    public void setTime(DateTime time) {
        _delta = DateTimes.deltaInMs(getRealNowInUtc(), time);
        _time = time;
    }

    public DateTime now() {
        if (_isFrozen) {
            return _time;
        }
        else {
            return getRealNowInUtc().minus(this._delta);
        }
    }

    public void reset() {
        _time = getRealNowInUtc();
        _delta = 0;
        _isFrozen = false;
    }

    private DateTime getRealNowInUtc() {
        return DateTime.now(DateTimeZone.UTC);
    }
}
