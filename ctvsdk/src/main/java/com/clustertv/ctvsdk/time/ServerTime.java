package com.clustertv.ctvsdk.time;

import org.joda.time.DateTime;

public class ServerTime {
    private static Clock _clock = new Clock();

    public static synchronized DateTime now() {
        return _clock.now();
    }

    public static synchronized void setTime(DateTime time) {
        _clock.setTime(time);
    }
}
